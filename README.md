# codeandsupply.fund

The website.

## Technical goals

1. It must load fast.
2. It must work on a wide range of browsers, especially mobile browsers.
3. It must be comprehensible to the widest possible audience.
4. It must use simple, understandable language.
5. It must be simple to use.
6. Its sharing tags must always function.

[Help make improvements](https://gitlab.com/codeandsupplyfund/codeandsupply.fund/issues?label_name[]=enhancement).

## Hugo how-to

0. Setup Hugo on a Mac with Homebrew using `make deps` in the repo directory.
1. Add pages in `content`.
2. See how it looks locally with `make watch`.

Run `make image-optim` after adding new images. Warning: it make take a while.

You may need to grab submodules for this project with `make submodulize`

## Testing sharing features

This site uses OpenGraph and Twitter card data extensively. Validate against
these testing services when making any changes to
layouts/partials/opengraph.html:

1. [Twitter](https://cards-dev.twitter.com/validator)
2. [Facebook](https://developers.facebook.com/tools/debug/sharing/) [quick
   test](https://developers.facebook.com/tools/debug/sharing/?q=https%3A%2F%2Fcodeandsupply.fund)
3. [iFramely](https://iframely.com/embed) [quick
   test](https://iframely.com/embed/https%3A%2F%2Fcodeandsupply.fund)

Ensure that the text shows up as expected as does the image.

