+++
title = "Board of Directors and other volunteering opportunities"
+++

Code and Supply Scholarship Fund needs you! As we grow, we identify
opportunities to streamline and expedite the process of turning donations into
successful outcomes.

## Board of Directors membership

Code and Supply Scholarship Fund, Inc. is a 501(c)(3) non-profit organization,
is looking for folks interested in joining its board of directors. Directors are
responsible for the long-term success and continuity of the organization. The
CSSF board is a "working board", in that as of 2019, the organization does not
have an executive team and performs all of the functions of the organization
itself.

The board is responsible for the current tasks:

* Finances, including donor and donation management
* Marketing and outreach to conferences
* Grantwriting
* Website maintainence
* Managing the selection committee

For 2019, we need the most help with:

* Software development: the board could use some assistance with projects small
  and large
* Consumer and Donor outreach: marketing to both individuals who may
  benefit from CSSF’s programs as well as individual donors who want to support
  those programs themselves
* Donor management duties: Help us manage our donor workflow in order to increase
  donor retention and frequency
* Foundation outreach: communication with foundations and more, including
  assisting in grant writing

What does board membership entail?

* **Time commitment.** This board meets approximately monthly. Participation via
  teleconference is permissible. Beyond these obligations, directors are expected
  to respond to email and chat activity between meetings and to devote some time
  monthly to contributing to the success of the organization. Directors serve
  three-year terms, but it is acceptable to resign early if life gets too busy
  to participate in board obligations.

* **Financial commitment.** Board members are expected to assist the organization
  financially through donations. Participation is more important than the
  amount, though. The board recognizes that not all contributions are monetary,
  so sweat equity will be taken into account when board donation participation
  is evaluated.

* **Compensation.** Directors are not paid for their work as directors, nor can they
  be employees of the organization, but they are permitted to perform work for
  the organization at a rate equivalent to what the organization would pay
  someone else for that work.

If you are interested in helping build CSSF into a robust scholarship-awarding
organization, let us know at
[scholarship@codeandsupply.co](mailto:scholarship@codeandsupply.co?subject=Board%20membership%20Inquiry)

## Open source projects

CSSF has a few open source projects for which we welcome contributors. See our
[GitLab organization](https://gitlab.com/codeandsupplyfund "Code and Supply Scholarship Fund on GitLab")
for the full listing but here are the ones most worth pointing out:

* [Scholarship Tracking System](https://gitlab.com/codeandsupplyfund/scholarship-tracking-system)
  - We want to automate the scholarship workflow, from accepting applications
  through periodic follow-up in the years after a conference experience.
* [codeandsupply.fund](https://gitlab.com/codeandsupplyfund/codeandsupply.fund)
  - our website is open source!
