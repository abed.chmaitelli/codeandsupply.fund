+++
title = "Scholarship qualifications and benefits"
+++

## What costs do CSSF scholarships cover?

A CSSF scholarship award may cover all or a part of a recipient's
**travel**,
**accommodations**, and
**registration fees**
for an event.

## What events can I attend?

CSSF scholarships fund attendance of events that meet these criteria:

* Topic focus
    * Software production
    * Software careers
    * Software community management and governance
* Location
    * in Pittsburgh, for any qualifying applicants
    * anywhere in the world, if the applicant is a presenter

Some conferences coordinate with CSSF to provide discounted or free registration.
Those pre-vetted conferences are identified on the application.

## Who may qualify for financial assistance?

If you will be

*   presenting to _or_ attending a software-related event in the [Pittsburgh metropolitan area](https://en.wikipedia.org/wiki/Pittsburgh_metropolitan_area), regardless of where you live, or
*   presenting to a software-related event anywhere in the world and you live in the [Pittsburgh metropolitan area](https://en.wikipedia.org/wiki/Pittsburgh_metropolitan_area),

then you may qualify. All qualifying applicants are considered, but awards are made on a case-by-case basis subject to available funds. Not all applicants will receive a scholarship award.

Code & Supply Scholarship Fund is presently targeting people internationally matching these descriptions:

1. People in financial need
    1. The unemployed. People who are currently not employed full-time or part-time in any position.
    2. The underemployed. People who are not currently employed in the field of their training.
        1.  A person having formal education in a related field but not employed in it presently
        2.  A person having an undergraduate degree in computer science but working full-time as a coffee shop barista while looking for work
    3. A retiree on a fixed income, regardless of their field during their career, as long as they are contributing to the software community at present
    4. Students
        1.  High school students who plan to enroll in a software-related degree program or are already working as a software professional
        2.  College students who are enrolled in a software-related degree program or have intent to seek employment in a relevant field following graduation regardless of degree program
2. Members of Underrepresented groups. People who are members of the following groups, which are commonly known to comprise a very small percentage of the professional roles in the software community:
    1.  Women
    2.  Black
    3.  Indigenous cultures, such as Native Americans
    4.  Hispanic/Latino
    5.  Gender, Sexual, and Romantic Minorities (GSRM)
    6.  Physically or mentally impaired
3. People seeking to enter the software profession
    1. A person who attained an English degree in their early 20s but decided to go back to school for computer science
    2. A person who is enrolled in a degree program for the first time and has been a software professional for less than two years
    3. A stay-at-home parent whose primary responsibility is the care of children but who intends to return to the profession when the children enter all-day schooling
    4. A person pursuing a degree part-time as a non-traditional student

C&S Scholarship Fund may award scholarships _worldwide_ to the above demographics provided that they:

1.  attend events within the greater Pittsburgh, Pennsylvania area, _or_
2.  live in the greater Pittsburgh, Pennsylvania area, but do not have the funding to speak at events to which they have been formally accepted to speak. These target recipients are expected to exhaust all other sources of funding, including employers, schools, public grants, and personal savings in excess of six months of expenses.

## Who do you consider "a presenter"?

We prioritize applications for presenters who are verifiably announced speakers: your name is or will be visible on a list of speakers on the event website. Attendees intending to sign-up on-site or chosen as alternates may qualify, as well.

## What is the application deadline for an event?

We strongly recommend submitting applications at least 30 days prior to an event.

Applications submitted within 30 days of an event may not be processed.

## What information do I need to apply?

The application will collect your name, email address, phone number, the details
of the event that you wish to attend including but not limited to date and location, 
from where you will be travelling to attend the event, your total requested
amount, detailed breakdown of the costs, and if you will be able to attend if you
receive only partial aid.

Lastly, you'll be asked a personal narrative question about what you hope to get
out of the event:

> How does your background influence your interaction with the tech community, 
> and why are you excited about attending this event?

Our selection committee takes these questions very seriously, especially the
narrative. 

Applicants must answer all questions truthfully. Questions should be directed to
[scholarship@codeandsupply.co](mailto:scholarship@codeandsupply.co?subject=Question+about+application).

## How to apply

Scholarship applications are open for limited events.

<span class="apply">
[Apply online](https://goo.gl/forms/IDOdU6KvVTATFH6R2)
</span>
