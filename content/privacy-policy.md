+++
title = "Scholarship Applicant and Donor Privacy Policy"
+++

## For Both

This policy applies to all information received by CSSF, online or offline, via
any platform, as well as any oral, written, or electronic communications.

Personally identifiable information given to CSSF will be used solely for the
purposes for which it was
collected, unless you have provide _explicit consent_ permitting CSSF to do
otherwise. CSSF may use information you provided to contact you in order to gain
consent to use your information for expanded purposes, unless you have
explicitly revoked permission to contact you.

Types of personally identifiable information collected may include, but are not
limited to:

* full name
* address
* email address and phone numbers

Information provided to CSSF may be stored by third-party vendors for CSSF's
exclusive use. When entering information on such a third-party vendor's website,
collection of that information may be governed by terms different from these.

## For Applicants and Awardees

CSSF will not share or sell personal information provided by any applicant,
regardless of the outcome of their application. Applicants are confidential.
Participation is voluntary but necessary to be considered for an award.

In addition to other personally identifiable information collected, applicants
may also be requested to submit:

* personal essays
* information contained on an IRS W-9 or W-8BEN form

When you submit an application, data you enter during the application process
may be automatically saved for your convenience. Even if the application is
abandoned or not submitted, the information you provide is collected and is
subject to this policy. Any data entered becomes the property of CSSF and is
governed by the laws of the Commonwealth of Pennsylvania and the United States
of America.

## For Donors

The privacy and security of both online and offline donors is of utmost 
importance to CSSF. CSSF protects this information in a variety of ways.

CSSF will not share or sell a donor's personal information with anyone else, nor
will it send donor mailings on behalf of other organizations unless the donor
provides explicit consent.

Donor information provided through a third-party donation processor will be used
only for purposes necessary to process the donation unless explicit consent is
given. Processing of payments and associated information, including by 
third-party vendors, is protected by industry standard safeguards.
