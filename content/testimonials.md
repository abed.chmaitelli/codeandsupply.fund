+++
title = "Testimonials from recipients"
+++

Scholarship Fund awards are not just about helping someone have an experience.
That experience leads to insight, opportunity, and self-improvement that can
yield new friends, new business contacts, new jobs, and even new careers. These
**outcomes** are what drives our donors to help others.

Are you a recipient and want to share your story? See how at the bottom of the page.

## Kelauni Cook

> Just 2.5 years ago, I moved to Pittsburgh to enter the tech industry as a
> former substitute teacher. Abstractions was the very first conference I'd
> attended in the city. Code & Supply embraced me as a "coding newbie" and
> understood that there was no way I'd be able to afford my ticket having moved
> here with just enough savings to afford my rent. I was able meet some amazing
> leaders in tech at Abstractions I, many of whom are still some of my greatest
> friends and mentors. But the cherry on top was my ability to network my way
> into **my very first software development job with The Washington Post**, which
> truly changed my life. Abstractions was awesome and I'm excited to now be able
> to pay that scholarship forward to another me at Abstractions II.

## James˚

Unemployed and discouraged after searching for a new job for several months, 
James˚ attended a conference using a CSSF award, found new enlightenment and 
rediscovered a love of the community, and was able to parlay that into a job 
where he was promoted within months of restarting his career at an employer that
more closely fit his culture expectations.

## Olivia Liddell

> In the Spring of 2018, I was thrilled to find out that I had been accepted to
> speak at the Heartifacts conference, and also that I had received a travel
> scholarship through the Code & Supply Scholarship Fund! I was excited to attend
> because I had never heard of such an event that specifically focused on the
> intersection between software development and mental health.
>
> At Heartifacts, I gave a talk on how to overcome fear of failure. The topic is
> very close to my heart because I’ve struggled with fear of failure and imposter
> syndrome for most of my life, and I was even afraid to submit my application for
> Heartifacts out of the fear that I would be rejected. 
>
> At the same time, I was also in the process of applying for new jobs. I had been
> working in the field of educational technology, and I was interested in finding
> a new role that would allow me to focus more on technical training. My first
> conference talk wasn’t recorded, so I decided to use my phone to make sure that
> I could get a recording of myself speaking at Heartifacts. Once I was back home
> in Chicago, I took a 7-minute clip of my talk, [uploaded it to YouTube as an
> unlisted video](https://www.youtube.com/watch?v=wfdEFb0UGWE), and then put the
> link onto my resume as I continued to apply for jobs.
>
> About a month later, I was in the final round of interviews at Cloudbakers, the
> company where I now work. As I met with several members of the team, including
> our CEO and CTO, everyone told me how watching that video of me speaking at
> Heartifacts played such a significant role in their decision to call me in for
> the interview… and then ultimately hire me. That also opened the door for me to
> speak with them about all of the other ways that I was positively impacted by my
> experience at Heartifacts, such as the importance of how to recognize and avoid
> burnout, and how to communicate in ways that are based on empathy and
> understanding.
>
> I’m incredibly grateful for the opportunity that I was given through the 
> Code & Supply Scholarship Fund. Being able to speak at Heartifacts truly changed
> my life for the better, and it has put me in a position where I am able to train
> and educate others, to help improve their lives, too.

## Anonymous, 2018 recipient

> As a student of a remote web development bootcamp living in a small town, 
> Code & Supply Scholarship Fund gave me the rare opportunity to meet and interact
> with real web developers in real life, for which I am very grateful. Without
> this scholarship, I would not have been able to network, make the contacts I
> made, or have exposure to new topics and ideas presented in the talks. I left
> the conference feeling more confident in my ability to be a successful
> professional in the web development industry. 

## Recipients, tell us your outcomes!

Are you a recipient of a Scholarship Fund award? Share your story with us one of
of these ways:

* Fill out our [testimonial survey](https://goo.gl/forms/SemdIt5CGYBih6Qm2 "Code and Supply Scholarship Fund Testimonial Survey")
* Submit your story on 
[this website's repository](https://gitlab.com/codeandsupplyfund/codeandsupply.fund).
* Send an email to [scholarship@codeandsupply.co](mailto:scholarship@codeandsupply.co)

----

_(˚some names changed for privacy reasons)_