+++
title = "Events we actively support"
+++

Listed below are some events that the Code & Supply Scholarship Fund its targetting for its scholarship awards.

Your event should be here if it's taking place in Pittsburgh. We try to keep
our ears to the ground but sometimes we miss events. [Let us know about
yours](mailto:scholarship@codeandsupply.co?subject=Event%20suggestion%3A%20XXXX&body=Hello%2C%20CSSF%21%20I%20have%20an%20event%20suggestion%20for%20you%3A%0A%0AEvent%20name%3A%0ADates%3A%0AWhere%3A%0A%0A%3C%21--%20If%20you%27re%20an%20organizer%20of%20the%20event%2C%20complete%20the%20below%21%20--%3E%0A%0A-%20%5B%20%5D%20We%20can%20provide%20X%20tickets%20for%20CSSF%20to%20distribute.%0A-%20%5B%20%5D%20We%20will%20mention%20CSSF%20and%20link%20to%20it%20from%20our%20web%20site.)
or [submit it on GitLab](https://gitlab.com/codeandsupplyfund/codeandsupply.fund/blob/master/content/events.md).

<!-- email template:

Hello, CSSF! I have an event suggestion for you:

Event name:
Dates:
Where:

----------- If you're an organizer of the event, complete the below!

- [ ] We can provide X tickets for CSSF to distribute.
- [ ] We will mention CSSF and link to it from our web site.

-->

**You can still [apply for a scholarship](/scholarships) to an event that is not listed here!**
This list is meant to inspire applicants to apply. It is not an exhaustive list
of events qualifying for CSSF support.

## Future events

| Day               | Conference                                       | Fees                                    | Notes            |
|-------------------|--------------------------------------------------|-----------------------------------------|------------------|
| 2019 August 21-23 | [Abstractions II](https://www.abstractions.io/)  | $50 student tickets, $300 GA tickets    | Official partner |
| 2019 August 22    | [Data Connectors](https://dataconnectors.com/events/pittsburgh2019/) | Free |  |

## Past events

| Dates            | Conference                                       | Fees                                    | Notes |
|------------------|--------------------------------------------------|-----------------------------------------|-------|
| 2019 June 28     | [BSides Pittsburgh](https://www.bsidespgh.com)   |                                         |       |
| 2019 March 28-30 | [Women in cybersecurity](https://www.wicys.net/) | [By member type](https://goo.gl/1vZFGN) |       |

