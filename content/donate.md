+++
title = "Donating to Code & Supply Scholarship Fund"
+++

## How to donate

Code & Supply Scholarship Fund (CSSF) is solely donor-funded as of 2018. That is,
a number of individuals, sometimes with [matching grants from their
employers](/matching/),
fund the awards that CSSF grants.

You can help us in a few ways.

### Direct cash donation

<span class="donation-link">
<i class="fa fa-money"></i>
{{< donorbox codeandsupplyfund-abstractions2 >}}Donate to our focused
campaign for 2019, Abstractions II{{</ donorbox >}}
</span>

Be sure to check the events bar at the top of the [main page](/) for event-specific
campaigns and other one-off opportunities.

We'd love if you submitted your donation to your company for [corporate
matching](/matching/).

### In-kind donations and other support

CSSF can accept a variety of in-kind donations:

* **Event tickets.** Cannot use a non-refundable ticket or just want to give
  someone else a great experience? Donate your ticket.
* **Rewards points.** These alternatives to cash donations are helpful since our
  scholarships often cover travel expenses.
  * Airline rewards points / miles
  * Hotel points
  * Rental car points

We can also accept cryptocurrency donations. Email us for an address.

### Corporate giving

We work with companies, foundations, and other non-profits interested in being
associated with supporting our target audience. We periodically host fundraising
events which are frequently supported by major corporate donors.

Contact us at [scholarship@codeandsupply.co](mailto:scholarship@codeandsupply.co)
with what interests you and we'll work something out.

If you'd like to make a donation, we encourage you to use the donation link above
for credit card donations up to $1,000. We prefer checks for donations exceeding
this and can issue invoices.

We strongly encourage companies to match their employees' donations. See more
about our [Matching program](/matching).

### Other donations

Contact CSSF if there are other things you can donate, such as a spare apartment
unit suitable for an awardee to use for a stay. Contact us with your idea and
we will see how it fits our audience's needs.

We can also accept cryptocurrency. Contact us for more information at
[scholarship@codeandsupply.co](mailto:scholarship@codeandsupply.co?subject=Cryptocurrency+donation)


## Tax implications

CSSF is a 501\(c)(3) non-profit organization. Donations are tax-deductible. You
will receive a receipt for tax purposes upon donation.

Donations made in-kind will be acknowledged but CSSF may not specify
a recognized deductible value. That value is up to a donor and their tax
professional to decide.

